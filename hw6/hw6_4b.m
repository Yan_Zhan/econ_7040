% logging
diary hw6_4a.log;

pi_0 = [0.5 ; 0.2 ; 0.3];

A = [0.8 0.1 0.1 ; 0.3 0.3 0.4 ; 0.3 0.6 0.1];

N = int64(1e1);

disp("pi_0 = ");
disp(pi_0);
disp("A = ");
disp(A);

for i = 1: N
    
    fprintf("i = %d\n", i);
    pi_0 = A * pi_0;
    disp(pi_0);
    
end

disp("When N = 10, pi_10 =")
disp(pi_0);

pi_0 =  [0.5 ; 0.2 ; 0.3];

A = [0.8 0.1 0.1 ; 0.3 0.3 0.4 ; 0.3 0.6 0.1];

N = int64(1e2);

for i = 1: N
    pi_0 = A * pi_0;
end

disp("When N = 100, pi_100 =");
disp(pi_0);

pi_0 = [0.5 ; 0.2 ; 0.3];
A = [0.8 0.1 0.1 ; 0.3 0.3 0.4 ; 0.3 0.6 0.1];
N = int64(1e4);

for i = 1:N
    pi_0 = A * pi_0; 
end    

disp("When N = 10000, pi_10000=");
disp(pi_0);

% turn off diary
diary off;
